alias Buildah.{Cmd, Print}

{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# from_name = System.fetch_env!("FROM_IMAGE")
# IO.puts(from_name)

# to_registry = System.fetch_env!("CI_REGISTRY_IMAGE")
# IO.puts(to_registry)

{parsed, args, invalid} = OptionParser.parse(System.argv(), strict: [tag: :string])
IO.puts(invalid)

OciAlpine.make_alpine_images(
    "docker.io",
    "alpine",
    "docker://" <> System.fetch_env!("CI_REGISTRY_IMAGE"),
    parsed
)

Print.images()
